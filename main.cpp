
#include <iostream>

using namespace std;

#include "Puzzle.h"

int main(int argc, char* argv[]) {
	Puzzle *puzzle;
	puzzle = new Puzzle(0);
	puzzle->parse("5 0 0 13 0 9 2 9 5 6 5");
	for (int i = 0; i < puzzle->num_of_vertices; i++) {
		puzzle->get(i)->print(' ');
		std::cout << " ";
	}
	cout << endl;
	for (int i = 0; i < puzzle->num_of_vertices; i++) {
		cout << puzzle->calculate_angle(i) << endl;
	}
	return 0;
}
