<?php

class GridLattice {

	private $x, $y;

	private $radius;

	private $diameter;

	private $line_spacing;

	private $image;

	private $dot_color;

	private $line_color;

	public function GridLattice($x, $y, $radius = 6, $line_spacing = 37) {
		$this->x = $x;
		$this->y = $y;
		$this->radius = $radius;
		$this->diameter = $radius * 2 + 1;
		$this->line_spacing = $line_spacing;
	// Calculate image dimesions
		$width = $this->line_spacing * ($this->x - 1) + $this->x * $this->diameter;
		$height = $this->line_spacing * ($this->y - 1) + $this->y * $this->diameter;
	// Build image
		$this->image = imageCreate($width, $height);
		$this->setDotColor(65, 65, 65);
		$this->setLineColor(255, 0, 0);
	}

	/**
	 * Draw grid lattice
	 *
	 * @return	void
	 */
	public function drawImage() {

		$white = imageColorAllocate($this->image, 255, 255, 255);

		imageFill($this->image, 0, 0, $white);

		for ($x = 0; $x < $this->x; $x++) {
			for ($y = 0; $y < $this->y; $y++) {
				$this->drawDot($x, $y);
			}
		}
	}

	/**
	 * Set color for the dot
	 */
	public function setDotColor($red = 65, $green = 65, $blue = 65) {
		$this->dot_color = imageColorAllocate($this->image, $red, $green, $blue);
	}

	public function setLineColor($red = 0, $green = 0, $blue = 0) {
		$this->line_color = imageColorAllocate($this->image, $red, $green, $blue);
	}	

	public function getCoordinate($index) {
		return ($this->line_spacing + $this->diameter) * $index + $this->radius;
	}

	public function drawDot($x, $y) {

		imageFilledEllipse(
			$this->image,
			$this->getCoordinate($x), $this->getCoordinate($y),
			$this->diameter, $this->diameter,
			$this->dot_color
		);
	}

	public function drawLine($x1, $y1, $x2, $y2, $thickness = -1) {

		// Set thickness for line
		if ($thickness == -1) {
			imageSetThickness($this->image, $this->radius);
		} else {
			imageSetThickness($this->image, $thickness);
		}

		// Draw line
		imageLine(
			$this->image,
			$this->getCoordinate($x1), $this->getCoordinate($y1), // Start point
			$this->getCoordinate($x2), $this->getCoordinate($y2), // End point
			$this->line_color
		);
	}

	public function flush($filename = null) {
		ob_start();
		imagePng($this->image, $filename);
		if ($filename == null) {
			header("Content-Type: image/png");
		}
		ob_end_flush();
	}

	public function destroy() {
		if ($this->image) {
			imageDestroy($this->image);
		}
	}
}
