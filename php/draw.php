<?php

require_once 'GridLattice.php';

$gl = new GridLattice(101, 65, 5, 30);
$gl->drawImage();
$output = file_get_contents("../output.txt");
$array = preg_split('/(\s+)/', $output);
$length = count($array);
if ($array[$length-1] == '') {
	unset($array[$length-1]);
	$length--;
}
$x = $array[0];
$y = $array[1];
for ($i = 1; $i < $length / 2; $i++) {
	$new_x = $array[$i*2];
	$new_y = $array[$i*2+1];
	$gl->drawLine($x, $y, $new_x, $new_y);
	$x = $new_x;
	$y = $new_y;
}
$gl->flush("../output.png");
print_r($array);