
#ifndef PUZZLE_H_DECLARATION

#define PUZZLE_H_DECLARATION

#include "Dot.h"
#include <cmath>
#include <sstream>
#include <algorithm>

struct Puzzle {
	
	int id;
	
	int num_of_vertices;
	
	Dot** vertices;
	
	double* angles;
	
	Puzzle(const int id);
	
	~Puzzle();
	
	void parse(const char* str);
		
	Dot* get(const int index);
	
	void set_vertice(const int index, Dot* dot);
	
	// Calculate the angle formed by vv1 and vv2
	double calculate_angle(const int v, const int v1, const int v2);
	
	double calculate_angle(const int v);
};

#endif
