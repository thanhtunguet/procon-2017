#ifndef DOT_H
#define DOT_H

#include <iostream>

struct Dot {

	int x;
	int y;

	Dot(const int x, const int y) {
		setX(x);
		setY(y);
	}

	void setX(const int x) {
		this->x = x;
	}
	

	void setY(const int y) {
		this->y = y;
	}

	void print(const char delimiter) {
		std::cout << this->x << " " << this->y;
	}
};

#endif
