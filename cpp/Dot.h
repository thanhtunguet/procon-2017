
#ifndef DOT_H_DECLARATION

#define DOT_H_DECLARATION

struct Dot {
	
	int x; // Horizontal position
	int y; // Vertical position
	
	Dot(const int x, const int y);
	
	void setX(const int x); // Set value for x
	
	void setY(const int y); // Set value for y
	
	// Print coordinates to screen
	void print(const char delimiter);
};

#endif
