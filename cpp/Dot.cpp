
#include "Dot.h"
#include <iostream>
Dot::Dot(const int x, const int y) {
	this->setX(x);
	this->setY(y);
}

void Dot::setX(const int x) {
	this->x = x;
}

void Dot::setY(const int y) {
	this->y = y;
}

void Dot::print(const char delimiter) {
	std::cout << this->x << delimiter << this->y;
}

