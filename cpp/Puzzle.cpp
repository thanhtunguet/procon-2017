
#include <cmath>
#include <sstream>
#include <algorithm>
#include <iostream>
#include "Puzzle.h"

Puzzle::Puzzle(const int id) {
	this->id = id;
}

Puzzle::~Puzzle() {
	for (int i = 0; i < this->num_of_vertices; i++) {
		delete vertices[i];
	}
	delete [] vertices;
	delete [] angles;
}

void Puzzle::parse(const char* str) {
	std::stringstream ss;
	ss << str;
	ss >> num_of_vertices;
	this->vertices = new Dot*[num_of_vertices];
	this->angles = new double[num_of_vertices];
	int x, y;
	int i = 0;
	while (ss >> x >> y) {
		vertices[i] = new Dot(x, y);
		i++;
	}
}

Dot* Puzzle::get(const int index) {
	return this->vertices[index];
}

void Puzzle::set_vertice(const int index, Dot* dot) {
	this->vertices[index] = dot;
}

double Puzzle::calculate_angle(const int v, const int v1, const int v2) {
	Dot *A, *B, *C;
	// Get the vertices
	A = vertices[v  % num_of_vertices];
	B = vertices[v1 % num_of_vertices];
	C = vertices[v2 % num_of_vertices];
	Dot *AB, *AC;
	AB = new Dot(B->x - A->x, B->y - A->y);
	AC = new Dot(C->x - A->x, C->y - A->y);
	double cosA;
	// Tich vo huong cua vector AB va AC
	int AB_AC = AB->x * AC->x + AB->y * AC->y;
	double ab, ac; // Do dai cua AB, AC
	ab = sqrt(AB->x * AB->x + AB->y * AB->y);
	ac = sqrt(AC->x * AC->x + AC->y * AC->y);
	cosA = (double) AB_AC / ab / ac;
	return acos(cosA) * 180 / M_PI;
}

double Puzzle::calculate_angle(const int v) {
	int v_next, v_prev;
	v_next = (v+1) % num_of_vertices;
	v_prev = (v-1+num_of_vertices) % num_of_vertices;
//	std::cout << "DEBUG " << v << " " << v_next << " " << v_prev << std::endl;
	double angle = calculate_angle(v, v_next, v_prev);
	{
		// So luot duyet cac goc con
		int iterations = num_of_vertices - 2;
		// Lay vi tri dau tien, duyet theo chieu tang dan
		
		double check_angle = 0;
		for (int i = 0; i < iterations; i++) {
			int v1 = (v_next + i) % num_of_vertices;
			int v2 = (v1 + 1) % num_of_vertices;
//			std::cout << "DEBUG " << v << " " << v1 << " " << v2 << std::endl;
			check_angle += calculate_angle(v, v1, v2);
		}
//	std::cout << "DEBUG " << check_angle << std::endl;
		if (check_angle <= 180) {
//			if (angle == check_angle) {
				return angle;
//			}
			
		} else {
			// Neu check_angle > 180 thi co 2 truong hop
			if (abs(check_angle + angle - 360.000) < 0.05) {
				return check_angle;
			} else {
				if (check_angle + angle > 360) {
					return check_angle;
				} else {
					return angle;
				}
			}
			
		}
	}
}
